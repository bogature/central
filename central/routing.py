
from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator

from central.consumers import *

application = ProtocolTypeRouter({

    "websocket": AllowedHostsOriginValidator(
        URLRouter([
            url(r'order/(?P<slug>[-\w]+)/$', ChatConsumer),
        ])
    ),

})
