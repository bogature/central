
from rest_framework.utils import json
from channels.generic.websocket import AsyncWebsocketConsumer
import json

from apps.order.models import Order


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        print('11111111111')
        self.room_name = self.scope['url_route']['kwargs']['slug']
        self.room_group_name = 'chat_%s' % self.room_name
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):

        print('2222222222')

        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def websocket_receive(self, message):
        print('333333333333333')

        # text_data_json = json.loads(message)
        # message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )


    # Receive message from room group
    async def chat_message(self, event):

        print('44444444')
        order = Order.objects.all()

        json_res = []

        # Iterate over results and add to array
        for record in order:
            json_obj = dict(start=record.start, finish=record.finish)
            json_res.append(json_obj)

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': json_res
        }))