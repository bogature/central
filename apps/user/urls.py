
from django.urls import path

from apps.user.views import *

urlpatterns = [

    path('', UserView.as_view()),

    path('<int:pk>/', UserView.as_view()),

    path('profile/<int:pk>/', ProfileView.as_view()),

]
