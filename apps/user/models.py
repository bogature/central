# coding=utf-8
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Country(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return u'%s' % self.name


class Sity(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True, blank=True)

    name = models.CharField(max_length=200)

    def __str__(self):
        return u'%s' % self.name


class Profile(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    sity = models.ForeignKey(Sity, on_delete=models.CASCADE, null=True, blank=True)

    phone = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return u'%s' % self.user


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

