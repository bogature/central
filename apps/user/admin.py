
from django.contrib import admin
from apps.user.models import *


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'user',)


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Country)
admin.site.register(Sity)
