# coding=utf-8
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Order(models.Model):

    start = models.CharField(null=True, blank=True, max_length=200)

    finish = models.CharField(null=True, blank=True, max_length=200)

    def __unicode__(self):
        return u'%s' % self.start