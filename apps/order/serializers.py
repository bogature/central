from django.contrib.auth import get_user_model
from rest_framework import serializers

from apps.order.models import Order
from apps.user.models import Profile

User = get_user_model()


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['id', 'start', ]
