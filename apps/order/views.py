from django.shortcuts import render

from apps.order.models import Order


def base_view(request):
    posts = Order.objects.all()
    return render(request, 'base.html', {'posts': posts})


def post_detail(request, slug):

    # post = Order.objects.get(id=slug)
    # form = CommentForm(request.POST or None)
    # comments = Order.objects.filter(post=post)

    return render(request, 'post_detail.html', {
        # 'post': post,
        # 'form': form,
        # 'comments': comments
    })
