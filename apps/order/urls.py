from django.conf.urls import url
from apps.order.views import *

urlpatterns = [
    url(r'^$', base_view, name='base_view'),
    url(r'order/(?P<slug>[-\w]+)/$', post_detail, name='post_detail')
]