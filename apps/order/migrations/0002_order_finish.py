# Generated by Django 2.2.6 on 2019-11-06 07:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='finish',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
